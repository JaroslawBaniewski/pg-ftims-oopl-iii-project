package projekt;

import java.util.ArrayList;

public interface Kierowanie {
    void odbierzZlecenie(Zlecenie zlecenie);
    void wypiszSumeZlecen();
    void obliczDziennyPrzychod(ArrayList<Zatrudniony> pracownicy);
    int wyznaczLiczbęStolarzy();
}
