package projekt;

public class Mebel {
    private static String[] rodzajeMebli = {"krzesło", "stół", "szafa", "biórko", "lóżko"};
    private static int zysk[] = {150, 550, 720, 330, 650}; // zysk (zł) ze sprzedarzy mebla (na czysto)
    private static int czas[] = {110, 210, 360, 185, 320}; // czas (min) produkcji mebla przez 1 stolarza
    private int rodzaj; // (0 - 4)

    public Mebel(int rodzaj) {
        this.rodzaj = rodzaj;
    }

    public int zwrocZysk() {
        return zysk[rodzaj];
    }

    public int zwrocRodzaj() {
        return rodzaj;
    }

    public int zwrocCzas() {
        return czas[rodzaj];
    }

    public String toString() {
        return rodzajeMebli[rodzaj];
    }
}
