package projekt;

public interface Sprzedarz {
    DokladneZlecenie odbierzZamowienie(Klient klient, Zlecenie zlecenie);
    void wypiszIloscOdebranychZlecen();
}
