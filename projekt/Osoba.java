package projekt;

import java.util.Random;

public class Osoba {
    private static String[] imiona = {"Anna", "Jan", "Maria", "Andrzej", "Katarzyna", "Piotr", "Małgorzata", "Krzysztof", "Agnieszka", "Stanisław",
                                        "Krystyna", "Tomasz", "Barbara", "Paweł", "Ewa", "Józef"};
    private static String[] nazwiska = {"Nowak", "Wójcik", "Kowalczyk", "Kaczmarek", "Mazur", "Zając", "Walczak", "Wieczorek", "Wróbel", "Motyka",
                                        "Żuk", "Zdunek", "Gałka", "Kępa", "Szyszka"};
    protected String imie;
    protected String nazwisko;
    protected Adres adres;

    public Osoba () {
        this("I", "N", new Adres());
    }

    public Osoba (String imie, String nazwisko, Adres adres) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
    }

    // generuje osobę o losowych parametrach
    public static Osoba wygenerujOsobe() {
        Random generator = new Random();
        Osoba nowaOsoba = new Osoba(imiona[generator.nextInt(imiona.length)], nazwiska[generator.nextInt(nazwiska.length)],
                            Adres.wygenerujAdres());
        return nowaOsoba;
    }

    public Adres zwrocAdres() {
        return adres;
    }

    public String toString() {
        return imie + " " + nazwisko + ", " + adres;
    }
}
