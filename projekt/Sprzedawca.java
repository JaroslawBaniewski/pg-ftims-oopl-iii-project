package projekt;

public class Sprzedawca extends Zatrudniony implements Sprzedarz {

    int iloscOdebranychZlecen;

    public Sprzedawca(Osoba osoba) {
        super(osoba);
        ustawPensje(12*8);
    }

    @Override
    public void wypiszIloscOdebranychZlecen() {
        System.out.println("sprzedawca przyjął " + iloscOdebranychZlecen + " zleceń");
    }

    @Override
    public DokladneZlecenie odbierzZamowienie(Klient klient, Zlecenie zlecenie) {
        DokladneZlecenie noweZlecenie = new DokladneZlecenie(zlecenie, klient);
        return noweZlecenie;
    }


}
