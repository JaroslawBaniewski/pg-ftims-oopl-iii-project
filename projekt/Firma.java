package projekt;

import java.util.*;

public class Firma {
    public static void main(String[] args) {
        int iloscStolarzy = 0; // wyliczana w zależności od zleceń
        int iloscZlecen = 5; // do modyfikacji
        ArrayList<DokladneZlecenie> zlecenia = new ArrayList<>();
        ArrayList<Zatrudniony> zatrudnieni = new ArrayList<>();
        Set<String> uliceDostawa = new HashSet<>();

        Sprzedawca sprzedawca = new Sprzedawca(Osoba.wygenerujOsobe());
        Kierownik kierownik = new Kierownik(Osoba.wygenerujOsobe());
        Dostawca dostawca = new Dostawca(Osoba.wygenerujOsobe());

        // stworzenie puli możliwych klientów
        int iloscKlientow = 10; // do modyfikacji
        ArrayList<Klient> klienci = new ArrayList<>();
        for (int i = 0; i < iloscKlientow; i++) {
            klienci.add(new Klient(Osoba.wygenerujOsobe()));
        }
        // wypisanie puli możliwych klientów
        for (int i = 0; i < klienci.size(); i++) {
            System.out.println(klienci.get(i));
        }
        System.out.println();

        // wygenerowanie określonej ilości zleceń składanych przez losowych klientów z puli
        for (int i = 0; i < iloscZlecen; i++) {
            Random generator = new Random();
            zlecenia.add(sprzedawca.odbierzZamowienie(klienci.get(generator.nextInt(klienci.size())), Zlecenie.generujZlecenie()));
            kierownik.odbierzZlecenie(zlecenia.get(i).zwrocZlecenie());
            // dodaje do zbioru ulice na ktora potrzebna bedzie dostawa (jeżeli nie została dodana wcześniej)
            if (!uliceDostawa.contains(zlecenia.get(i).zwrocKlienta().zwrocAdres().zwrocUlice()))
                uliceDostawa.add(zlecenia.get(i).zwrocKlienta().zwrocAdres().zwrocUlice());
        }

        // wydruk zleceń
        for (int i = 0; i < iloscZlecen; i++) {
            System.out.println(zlecenia.get(i));
        }
        System.out.println();


        // ilość stolarzy wyznaczana jest na podstawie czasu wytwarzania mebli ze wszystkich zleceń aby praca nie zajęła więcej niż 8 godzin
        iloscStolarzy = kierownik.wyznaczLiczbęStolarzy();
        zatrudnieni.add(kierownik);
        zatrudnieni.add(sprzedawca);
        zatrudnieni.add(dostawca);
        for (int i = 0; i < iloscStolarzy; i++)
            zatrudnieni.add(new Stolarz(Osoba.wygenerujOsobe()));

        // wypisuje ile sztuk konkretnych mebli zamówiono łącznie
        kierownik.wypiszSumeZlecen();
        System.out.println();

        // wypisuje sumę przeznaczoną na pensje, zarobek z mebli oraz zarobek po odliczeniu pieniędzy na pensje
        kierownik.obliczDziennyPrzychod(zatrudnieni);
        System.out.println();

        // wypisuje wszystkich zatrudnionych wraz ze stanowiskiem
        for (Zatrudniony x: zatrudnieni) {
            System.out.println(x);
        }
        System.out.println();

        // wypisuje ze zbioru wyprodukowane rodzaje mebli z użyciem iteratora
        Iterator iterator = uliceDostawa.iterator();
        System.out.println("Dostawa na ulice: ");
        while (iterator.hasNext())
            System.out.println(iterator.next());

        System.out.println();
        try {
            System.out.println("Ilość wyprodukowanych krzeseł: " + kierownik.zwrocIloscWyprodukowanychMebli(0));
        } catch (ZleIdException e) { System.out.println("Nie sprawdzono ilości wyprodukowanych mebli.."); }

        System.out.println();
        try {
            System.out.println("Ilość wyprodukowanych krzeseł: " + kierownik.zwrocIloscWyprodukowanychMebli(10)); // błąd: 10 zamiast 0
        } catch (ZleIdException e) { System.out.println("Nie sprawdzono ilości wyprodukowanych mebli.."); }
    }
}
