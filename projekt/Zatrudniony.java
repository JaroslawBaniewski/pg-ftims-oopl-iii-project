package projekt;

public class Zatrudniony extends Osoba {

    private int pensja;

    public Zatrudniony(Osoba osoba) {
        this.imie = osoba.imie;
        this.nazwisko = osoba.nazwisko;
        this.adres = osoba.adres;
    }

    public Zatrudniony() {

    }

    public int zwrocPensje() {
        return pensja;
    }

    public void ustawPensje(int pensja) {
        this.pensja = pensja;
    }

    public String toString() {
        return getClass().getSimpleName() + " " + super.toString();
    }
}
