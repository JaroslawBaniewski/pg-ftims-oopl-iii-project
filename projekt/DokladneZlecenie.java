package projekt;

public class DokladneZlecenie {
    private Zlecenie zlecenie;
    private Klient klient;

    public DokladneZlecenie(Zlecenie zlecenie, Klient klient) {
        this.zlecenie = zlecenie;
        this.klient = klient;
    }

    public Zlecenie zwrocZlecenie() {
        return zlecenie;
    }

    public Klient zwrocKlienta() { return klient; }

    public String toString() {
        return zlecenie + ", " + klient;
    }
}
