package projekt;

import java.util.Random;

public class Adres {
    private static String[] ulice = {"Stalowa", "Jagielońska", "Targowa", "Marsa", "Grochowska", "Wolska", "Mickiewicza", "Słowackiego",
                                    "Świętokrzyska", "Marszałkowska", "Belwederska"};
    private static String miasto = "Warszawa";
    private String ulica;
    private int numerDomu;

    public Adres() {
        this("U", 0);
    }

    public Adres (String ulica, int numerDomu) {
        this.ulica = ulica;
        this.numerDomu = numerDomu;
    }

    // pozwala wygenerować losowy adres
    public static Adres wygenerujAdres() {
        Random generator = new Random();
        Adres nowyAdres = new Adres(ulice[generator.nextInt(ulice.length)], (generator.nextInt(200) + 1));
        return nowyAdres;
    }

    public String zwrocUlice() {
        return ulica;
    }

    public String toString() {
        return "adres: " + miasto + ", ul. " + ulica + " " + numerDomu;
    }
}
