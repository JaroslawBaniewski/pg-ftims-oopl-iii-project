package projekt;

import java.util.ArrayList;

public class Kierownik extends Zatrudniony implements Kierowanie{
    private ArrayList<Zlecenie> sumaZlecen = new ArrayList<>();
    //private ArrayList<Zatrudniony> zatrudnieni;

    public Kierownik(Osoba osoba) {
        super(osoba);
        ustawPensje(30*8);
        for (int i = 0; i < 5; i++) {
            sumaZlecen.add(new Zlecenie(new Mebel(i), 0));
        }
    }

    // odebrane zlecenie trafia do sumy zleceń
    @Override
    public void odbierzZlecenie(Zlecenie zlecenie) {
        int i = 0;
        while(sumaZlecen.get(i).zwrocMebel().zwrocRodzaj() != zlecenie.zwrocMebel().zwrocRodzaj()) {
            i++;
        }
        sumaZlecen.get(i).zwiekszIlosc(zlecenie.zwrocIlosc());
    }

    @Override
    public void wypiszSumeZlecen() {
        System.out.println("Suma wszystkich zleceń: ");
        for (int i = 0; i < 5; i++) {
            if (sumaZlecen.get(i).zwrocIlosc() != 0)
                System.out.println(sumaZlecen.get(i));
        }
    }

    @Override
    public void obliczDziennyPrzychod(ArrayList<Zatrudniony> pracownicy) {
        int pensje = 0;
        int zarobek = 0;
        for (Zatrudniony x: pracownicy) {
            pensje += x.zwrocPensje();
        }
        System.out.println("Pieniądze przeznaczone na pensje: " + pensje + " zł");
        for (Zlecenie x: sumaZlecen) {
            zarobek += x.zwrocKwote();
        }
        System.out.println("Zarobek z mebli: " + zarobek + " zł");

        System.out.println("Dzienny przychód firmy wynosi: " + (zarobek - pensje) + " zł");
    }

    // zwraca liczbę stolarzy na podstawie czasu produkcji wszystkich mebli
    @Override
    public int wyznaczLiczbęStolarzy() {
        int czas = 0;
        int ilosc = 0;
        for (Zlecenie x: sumaZlecen) {
            czas += x.zwrocMebel().zwrocCzas();
        }
        ilosc = czas / (60 * 8);
        if (czas % (60 * 8) != 0)
            ilosc++;
        return ilosc;
    }

    // zwraca ilość wyprodukowanych mebli danego typu
    public int zwrocIloscWyprodukowanychMebli(int mebelID) throws ZleIdException {
        int ilosc = 0;
        if (mebelID < 0 || mebelID > 4)
            throw new ZleIdException();
        else {
            for (Zlecenie x: sumaZlecen) {
                if (x.zwrocMebel().zwrocRodzaj() == mebelID)
                    ilosc = x.zwrocIlosc();
            }
        }
        return ilosc;
    }
}
