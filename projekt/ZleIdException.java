package projekt;

// wyjątek występuje przy próbie dostępu do danych o meblu o nieprawidłowym indeksie
public class ZleIdException extends Exception {

    @Override
    public String getMessage() {
        return "OSTRZEŻENIE: mebel o podanym ID nie istnieje!!!";
    }
}
