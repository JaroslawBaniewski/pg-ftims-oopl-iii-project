package projekt;

import java.util.Random;

public class Zlecenie {
    private Mebel mebel;
    private static final int kosztDostawy = 50;
    private int ilosc;

    public Zlecenie() {
        this(null, 0);
    }

    public Zlecenie(Mebel mebel, int ilosc) {
        this.mebel = mebel;
        this.ilosc = ilosc;
    }

    public int zwrocKwote() {
        return ilosc * mebel.zwrocZysk() - kosztDostawy;
    }

    public int zwrocIlosc() {
        return ilosc;
    }

    public Mebel zwrocMebel() {
        return mebel;
    }

    public void zwiekszIlosc(int ile) {
        ilosc += ile;
    }

    // generuje losowe zlecenie
    public static Zlecenie generujZlecenie() {
        Random generator = new Random();
        Zlecenie noweZlecenie = new Zlecenie(new Mebel(generator.nextInt(5)), generator.nextInt(5) + 1);
        return noweZlecenie;
    }

    public String toString() {
        return "zlecenie: " + ilosc + " x " + mebel;
    }
}
